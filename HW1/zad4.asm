LIST p=18f1220		; target processor
CONFIG PWRT = ON, WDT = OFF, MCLRE = OFF
; microcontroller configuration
; WDT = off	--watchdog timer
; PWRT = on	-- Power Up Timer
; MCLRE = off	-- internally connect ~MCLR to Vcc

#include "P18f1220.INC"			; file with constants definitions for the PIC

Ram		EQU		h'0'
state	EQU		Ram+0
l1		EQU		Ram+1
l2		EQU		Ram+2

	       org		0			; Mandatory - program begins from address 0
	       clrw					; Clear working register
	       clrf		LATB		; Clear latch B since we don't wanat to write anything just yet
	       clrf		TRISB		; All 0 - all ports are set as output
	       
	       clrf		ADCON0		; Turn off A/D
	       movlw	0xff			; All pins are digital
	       movwf	ADCON1		; Set all pins to digital
	       
	       clrf		state		; Clear state
	       
loop		call		getmask		; Get the bitmask for the current state
		movwf	LATB		; Write the mask to the latch port B
		incf		state,W		; Increment state and write it to the Working Register
		andlw	0x03		; 03 = 11(b) - get just the lowest 2 bits -- basically the modulo 4 operation
		movwf	state		; Write our incremented and modulo-ed result into the state register
		call		wait			; Have to chill a bit now
		goto		loop			; Repeat ad infinum
		
getmask	movf	state, W
		addwf	state, 0    ; Since ROM is 2-bytes width we have to multiply state by 2
		addwf	PCL,1	; LUT jumping
		retlw	b'00100001' ; 010 0 001	; state==0 green and red
		retlw	b'00010011' ; 000 0 011	; state==1 orange and red/orange
		retlw	b'00001100'   ; 000 1 100	; state==2 red and green
		retlw	b'00011010' ; 001 0 010 	; state==3 red/orange and orange
		
wait		movlw	5
		movwf	l1
w1		call		wait2
		decfsz	l1			; decrement file register, skip if zero
		goto		w1
		return
		
wait2	clrf		l2
w2		decfsz	l2
		goto		w2
		return
		END