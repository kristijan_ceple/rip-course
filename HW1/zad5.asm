LIST p=18f1220		; target processor
CONFIG PWRT = ON, WDT = OFF, MCLRE = OFF
; microcontroller configuration
; WDT = off	--watchdog timer
; PWRT = on	-- Power Up Timer
; MCLRE = off	-- internally connect ~MCLR to Vcc

#include "P18f1220.INC"			; file with constants definitions for the PIC

Ram		EQU		h'0'
state	EQU		Ram+0
l1		EQU		Ram+1
l2		EQU		Ram+2

	       org		0			; Mandatory - program begins from address 0
	       goto init
	       
	       org 8				; Interrupt vector
inte
	       decfsz state, 1			; Skip next line if zero! If not zero go to next line!
	       goto check_state			;  Not zero - need to go ahead to the mask getting part, but BEFORE that check if we're in state 5(101)!
	       movlw 0x6				; State zero needs to be set back to 6
	       movwf state
	       goto inte_if_get_mask
check_state
		movlw 0x5
		xorwf state, 0
		BZ 0x20 		; If state is 5(101) then skip next line and wait instead. If state is not 5, then go to next line and from there take it to mask getting
		goto inte_if_get_mask
		call getmask	; First update display, then sleep, and then go into the next state after the sleep duration
		movwf LATB
		call wait		; Sleep via 2 loops
		goto inte		; After the time passes, we immediately have to go to the next state
inte_if_get_mask
	       call getmask				; Now update the mask/display!
	       movwf LATB

	       bcf INTCON3, INT2IF		; We've processed this interrupt, so lower the flag for it
	       retfie FAST
	       
	       
init	    
	       clrw					; Clear working register
	       
	       ;bcf		STATUS, RP0	; Select bank 0
	       ;bcf		STATUS, RP1	; Select bank 0
	       
	       clrf		LATB		; Clear latch B since we don't wanat to write anything just yet
	       clrf		PORTB		; Clear Port B just in case
	       
	       clrf		TRISB		; All 0 - all ports are set as output
	       bsf		TRISB, 2		; Set pin17 as input
	       bsf		INTCON, GIE	; Enable global interrupts
	       bsf		INTCON3, INT2IE
	       ;bsf		INTCON, RBIE
	       
	       clrf		ADCON0		; Turn off A/D
	       movlw	0xff			; All pins are digital
	       movwf	ADCON1		; Set all pins to digital
	       
	       clrf		state		; Clear state
	       movlw	0x6
	       movwf	state		; Init state to 6

program	
	       call getmask			; Update mask for the starting state
	       movwf LATB
	      
loop
		  goto loop			; Do nothing, interrupts will now change states and update lights
		
getmask	movf	state, W
		;addlw	-0x01		; Since states are from [1,6] we want to map them into [0,5] range due to PLC jumping
		addwf	state, 0    ; Since ROM is 2-bytes wide we have to multiply state by 2
		addwf	PCL,1	; LUT jumping
		NOP
		retlw	b'00000010' ; 001 0 010 	; state==5 Orange Cars and Red Pedestrians		-- on click
		retlw	b'00010000' ; 001 0 010 	; state==4 Green Cars and Red Pedestrians		-- on click
		retlw	b'00000011' ; 001 0 010 	; state==3 Red-Orange Cars and Red Pedestrians	-- on click
		retlw	b'00000001'  ; 000 1 100	; state==2 Red Cars and Red Pedestrians			-- on click
		retlw	b'00100001' ; 000 0 011		; state==1 Red Cars and Green Pedestrians		-- timed
		retlw	b'00000001' ; 010 0 001		; state==0 Red Cars and Red Pedestrians			-- on click
		
wait		movlw	5
		movwf	l1
w1		call		wait2
		decfsz	l1			; decrement file register, skip if zero
		goto		w1
		return
		
wait2	clrf		l2
w2		decfsz	l2
		goto		w2
		return
		END