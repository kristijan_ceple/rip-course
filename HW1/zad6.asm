LIST p=18f1220		; target processor
CONFIG PWRT = ON, WDT = OFF, MCLRE = OFF
#include "P18f1220.INC"

Ram 							EQU 			h'0'
state							EQU			Ram + 0
car_arrived				EQU			Ram + 1
critical_speed_kmh 	EQU 			Ram + 4
critical_speed_dig 	EQU 			Ram + 5
alarm_reg 					EQU 			Ram + 8
dest 							EQU 			Ram + 0x10

;ad_reg_1 equ Ram+6
;ad_reg_2 equ Ram+7


						org				0x0
						goto			init


						org 0x8
						goto button_hp_int		; High priority interrupt vector
						org 0x18
						goto ad_lp_int				; Low priority interrupt vector

button_hp_int	
						btg car_arrived, 0		; Invert the car arrived bool
						bcf INTCON3, INT2IF	; Clear the INT2 flag
						retfie FAST					; return from interrupt
	
ad_lp_int
						; Interrupt code here!
						;movf ADRESH, W	; Upper 2 bits of ADC
						TSTFSZ  ADRESH
						goto ad_lp_int_alarm ; Here it's NOT zero so we need to turn on the alarm
						 ; ZERO, maybe no alarm, need to test the lower part
						movf ADRESL, W		; Lower 8 bits of ADC
						subwf critical_speed_dig, W
						BC	no_alarm								; If res > 0 then speed is fine because SUBWL does  kk - WL and if C = 0 then kk > WL 
ad_lp_int_alarm
						bsf alarm_reg, 0

						goto ad_lp_int_end
no_alarm		
						bcf alarm_reg, 0
ad_lp_int_end
						bcf PIR1, ADIF			; Clear ADC's Interrupt Flag
						retfie FAST	

init
						movlw d'60'
						movwf critical_speed_kmh	; Init critical speed value

						movlw d'178'
						movwf critical_speed_dig	; Init critical speed value

						bsf RCON, IPEN		; enable low priority interrupts
						movlw b'00000101'	; set rb0 and rb2 as inputs
						movwf TRISB
						movlw b'00010001'	; Configuring ADC
						movwf ADCON0
						
						movlw b'01101111'
						movwf ADCON1		; Configuring ADC
						movlw	b'10001101'
						movwf ADCON2		; Configuring ADC

						bsf PIE1, ADIE			; turn on AD interrupts
						bcf IPR1, ADIP			; set AD/peripheral to low priority
						bsf ADCON0, GO		; start conversion

						bsf INTCON3, INT2IE		; Enable interrupts on INT2,
						bsf IPR1, INT2IP				; Even though this is automatically set, leaving it here for clarity
						movlw b'11001000'
						movwf INTCON
			
loop				; Check if car has arrived!
						btfss car_arrived, 0
						goto loop_turn_off_alarm

						; Check speed!
						bsf ADCON0, GO		; start conversion
						btfss alarm_reg, 0
						goto loop_turn_off_alarm 

loop_turn_on_alarm
						setf dest
						movlw b'00010000'
						goto loop_end

loop_turn_off_alarm
						clrf dest
						bcf alarm_reg, 0		; No car, no alarm!
						movlw b'00000000'
						
loop_end
						movwf LATB
						goto loop
						END