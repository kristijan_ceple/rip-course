LIST p=18f1220
CONFIG PWRT = ON, WDT = OFF, MCLRE = OFF
#include "P18f1220.INC"

RAM EQU 0x00
l1 EQU RAM + 0x01
l2 EQU RAM + 0x02
SPEED_A_CNT EQU RAM + 0x03

ORG 0
GOTO INIT


ORG 8
SUBROUTINE
MOVLW 0X01
MOVWF l1
BCF INTCON3, INT2IF		; We've processed this interrupt, so lower the flag for it
RETFIE  FAST


INIT
CLRW
CLRF LATB	; Clear latch B since we don't want to write anything just yet
CLRF PORTB	; Clear Port B just in case

CLRF TRISB		; All 0 - all ports are set as output
BSF INTCON, GIE		; Enable global interrupts
BSF INTCON3, INT2IE		; Enable interrupts on INT2 pin

CLRF ADCON0		; Turn off A/D
MOVLW 0xFF	; Set all pins as digital
MOVWF ADCON1

CALL DELAY	; Chill a bit so the user can see the changes

ONE_SEMAPHORE_CYCLE_THROUGH
; Have to make one semaphore go through all the states and then synchronise the 2 semaphores
; Let's speed up semaphore A at pin 8 - RB0 -- send 3 changes to it!
MOVLW 0X02
MOVWF SPEED_A_CNT

SPEED_A
SPEED_A_LOOP
; Send impulse
; Turn it on here, and then wait a bit
MOVLW b'00000001'
MOVWF LATB
CALL DELAY

; Turn off the impulse, and then wait a bit
MOVLW b'00000000'
MOVWF LATB
CALL DELAY

DECFSZ SPEED_A_CNT
GOTO SPEED_A_LOOP

SEMAPHORES_SYNCHRONISATION
MOVLW b'00000001'
MOVWF LATB
CALL DELAY
MOVLW b'00000000'
MOVWF LATB
CALL DELAY

CALL ONE_CYCLE
CALL ONE_CYCLE

MOVLW b'00000010'
MOVWF LATB
CALL DELAY
MOVLW b'00000000'
MOVWF LATB
CALL DELAY

CALL ONE_CYCLE
CALL ONE_CYCLE
GOTO SEMAPHORES_SYNCHRONISATION


ONE_CYCLE
MOVLW b'00000011'
MOVWF LATB
CALL DELAY

MOVLW b'00000000'
MOVWF LATB
CALL DELAY
RETURN


DELAY
movlw	5
movwf	l1
w1		
call		wait2
decfsz	l1			; decrement file register, skip if zero
goto		w1
return
		
wait2	
clrf		l2
w2		
decfsz	l2
goto		w2
return
END