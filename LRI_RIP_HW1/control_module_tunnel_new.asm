LIST p=18f1220
CONFIG PWRT = ON, WDT = OFF, MCLRE = OFF
#include "P18f1220.INC"

RAM EQU 0x00
l1 EQU RAM + 0x01
l2 EQU RAM + 0x02
PHASE_REDUCTION_ENABLED  EQU RAM + 0x03


ORG 0
GOTO INIT


ORG 8
BUTTON_SUBROUTINE

; Check if bool is toggled on!
BTFSS PHASE_REDUCTION_ENABLED, 0
GOTO BUTTON_SUBROUTINE_END
MOVLW 0X01
MOVWF l1
BUTTON_SUBROUTINE_END
BCF INTCON3, INT2IF		; We've processed this interrupt, so lower the flag for it
RETFIE  FAST


INIT
CLRF LATB	; Clear latch B since we don't want to write anything just yet
CLRF PORTB	; Clear Port B just in case

CLRF TRISB		; All 0 - all ports are set as output
BSF INTCON, GIE		; Enable global interrupts
BSF INTCON3, INT2IE		; Enable interrupts on INT2 pin

CLRF ADCON0		; Turn off A/D
MOVLW 0xFF	; Set all pins as digital
MOVWF ADCON1


SPEED_A
; Have to make one semaphore go through all the states and then synchronise the 2 semaphores
; Let's speed up semaphore A at pin 8 - RB0 -- send 3 changes to it!
; Sem A - state 0
SETF  PHASE_REDUCTION_ENABLED
CALL DELAY	; Chill a bit so the user can see the changes
CLRF PHASE_REDUCTION_ENABLED
CALL A_IMPULSE		; Change the lights
; Sem A - state 1
CALL DELAY			; Chill - wait for the Pedestrians light to turn itself off automatically
SETF PHASE_REDUCTION_ENABLED
; Sem A - state 2
CALL DELAY			; We're in a new state - Pedestrians light has changed and is turned off - therefore chill a bit
CALL A_IMPULSE		; Go to next state - change lights
; Sem A - state 3
CALL DELAY			; Stay a bit in that next state

; This delay's duration must NOT be changed since it happens simultaneously with the green light delay that is hard-coded into the semaphore(which we must not touch).
; Beside, Pedestrians do not want to shorten their own Passing time
CLRF PHASE_REDUCTION_ENABLED			; Disable Phase Reductions
CALL BOTH_IMPULSE		; Both semaphores have to go into the next state - change both of their lights
; Sem A - state 4
CALL DELAY					; Chill a bit in this new state


SEMAPHORES_SYNCHRONISATION
; Semaphore B will switch its own state(Pedestrian lights), but semaphore A will not - so we have to do it manually here
CALL A_IMPULSE	
SETF PHASE_REDUCTION_ENABLED			; Phases can be once again skipped
; Sem A - state 5
CALL DELAY		; Both Sem A and B states/lights changed - so chill a bit in this new state

CALL CHANGE_BOTH_STATE_AND_WAIT			; Go into new state and stay there a bit - Sem A state 0 done
CLRF PHASE_REDUCTION_ENABLED
CALL CHANGE_BOTH_STATE_AND_WAIT			; Go into new state and stay there a bit - Sem A state 1 done

; Sem A will put itsef into the next state automatically(Pedestrian lights) but we have to take care of Sem B manually
CALL B_IMPULSE
SETF PHASE_REDUCTION_ENABLED
; Sem A - state 2
CALL DELAY		; Stay in this new state a bit

CALL CHANGE_BOTH_STATE_AND_WAIT			; Change state and stay a bit - Sem A state 3 done
CLRF PHASE_REDUCTION_ENABLED
CALL CHANGE_BOTH_STATE_AND_WAIT			; Change state and stay a bit - Sem A state 4 done
GOTO SEMAPHORES_SYNCHRONISATION			; Once again Sem B(P. lights) will change its own state automatically, but we have to change state A manually - go to the beginning of the loop!




CHANGE_BOTH_STATE_AND_WAIT
CALL BOTH_IMPULSE
CALL DELAY
RETURN


A_IMPULSE
MOVLW b'00000001'
MOVWF LATB
MOVLW b'00000000'
MOVWF LATB
RETURN

B_IMPULSE
MOVLW b'00000010'
MOVWF LATB
MOVLW b'00000000'
MOVWF LATB
RETURN

BOTH_IMPULSE
MOVLW b'00000011'
MOVWF LATB
MOVLW b'00000000'
MOVWF LATB
RETURN

DELAY
movlw	5
movwf	l1
w1		
call		wait2
decfsz	l1			; decrement file register, skip if zero
goto		w1
return
		
wait2	
clrf		l2
w2		
decfsz	l2
goto		w2
return
END