#include <p18f1220.h>
#pragma config WDT = OFF
#pragma config PWRT = ON
#pragma config MCLRE = OFF
#define _50ppm 0x66 // koncentracija od 50ppm-a - 102 (10)
#define _70ppm 0x8f // koncentracija od 70ppm-a - 143 (10)
unsigned char maxCO;
void high_isr(void);            // prototip prekidne rutine
#pragma code high_vector = 0x08 // direktiva pretprocesoru – slijedi
// prekidni vektor
void interrupt_at_high_vector(void)
{
    _asm GOTO high_isr _endasm // skoci na prekidnu rutinu
}
#pragma code               // kraj prekidnog vektora
#pragma interrupt high_isr // direktiva pretprocesoru - slijedi
// prekidna rutina visokog prioriteta
void high_isr(void)
{
    if (PIR1 & 0x40) // prekid od A/D-a
    {
        if (ADRESL < _50ppm)
        {
            PORTA &= 0xf7; // ugasi ventilator
        }
        else if (ADRESL > _70ppm)
        {
            PORTA |= 0x0a; // upali ventilator i alarm
            if (maxCO < ADRESL)
                maxCO = ADRESL; // zapamti najvišu koncentraciju
        }
        else
        {
            PORTA |= 0x08; // upali samo ventilator
        }
        PIR1 &= 0xbf; // obrisi ADIF
        ADCON0 |= 2;  // pokreni novu pretvorbu
    }
    else if (INTCON & 0x02) // prekid od tipke / INTOIF podignut?
    {
        PORTA &= 0xfd;  // ugasi alarm
        maxCO = 0;      // resetiraj maksimalnu vrijednost
        INTCON &= 0xfd; // spusti prekidnu zastavicu INTOIF
    }
}
#pragma code // kraj programskog koda prekidne rutine
void main(void)
{
    PORTA = 0;     // obrisi port A, moze i LATA = 0;
    TRISA = 0xf5;  // izlazi RA1, RA3: 0101, ostali ulazni
    PORTB = 0;     // obrisi port B, moze i LATB = 0;
    TRISB = 0xff;  // sve linije ulazne, koristimo samo najnizu (prekidnu)
    ADCON0 = 0x01; // 00 0 000 0 1 ; AN0, INTREF, ADON
    ADCON1 = 0xfe; // AN0 je analogni ulaz, ostali portovi su digitalni
    ADCON2 = 0x96; // 1 0 010 110 - right justified, 4 TAD, Fosc/64
    PIR1 = 0;      // obrisi zastavice prekida (ADIF) - treba i u prekidnoj rutini
    PIE1 = 0x40;   // omoguci prekid od A/D-a, ADIE = 1
    INTCON = 0xd0; // GIE=1, PEIE=1, INT0IE = 1, INT0IF = 0
    ADCON0 |= 2;   // pokreni pretvorbu
    maxCO = 0;     // pocetna vrijednost maksimalne koncentracije = 0
    while (1)
    {
    } // beskonacna petlja
}