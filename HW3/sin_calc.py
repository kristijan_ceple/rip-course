from math import sin, pi

N = 32
_2_A = 1020


def compute_function(i):
    return int((sin(2*pi*i/N) + 1) * _2_A/2)


def main():
    for i in range(N):
        print(f"{compute_function(i)}", end=", ")

    print()
    print((250*1000)/(3*N*1))


if __name__ == "__main__":
    main()
